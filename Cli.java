package ui;

import java.util.Scanner;

import tp.Polynomial;

public class Cli {

	public static void main( String args[] ) {
		Scanner scanner = new Scanner( System.in );
		Polynomial polynomial1 = new Polynomial( scanner.nextLine() );
		System.out.println( polynomial1 );
		Polynomial polynomial2 = new Polynomial( scanner.nextLine() );
		System.out.println( polynomial2 );
		polynomial2.divide( polynomial1 );
		System.out.println( polynomial2 );
		
	}
	
}
