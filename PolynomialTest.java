package tp;

import static org.junit.Assert.*;

import org.junit.Test;

public class PolynomialTest {

	Polynomial poly2,res,poly1;
	
	@Test
	public void testAddPolynomial() {
		poly1 = new Polynomial( "1x^2+5x^1-10x^0" );
		poly2 = new Polynomial( "2x^2-5x^1" );
		poly1.add( poly2 );
		res = new Polynomial( "3x^2-10x^0" );
		System.out.println( poly1 );
		System.out.println(res);
		if( !poly1.equals( res ) )
			fail( "Not equal" );
	}

	@Test
	public void testMultiplyPolynomial() {
		poly1 = new Polynomial( "1x^2+5x^1-10x^0" );
		poly2 = new Polynomial( "2x^2" );
		poly1.multiply( poly2 );
		res = new Polynomial( "2x^4+10x^3-20x^2" );
		if( !poly1.equals( res ) )
			fail( "Not equal" );
	}

	@Test
	public void testDerivate() {
		poly1 = new Polynomial( "3x^2+5x^1-10x^0" );
		poly1.derivate();
		res = new Polynomial( "6x^1+5x^0" );
		if( !poly1.equals( res ) )
			fail( "Not equal" );
	}

}
