package algorithms;

public class FancyScanner {
	private String str;
	private int pointer;
	public FancyScanner( String s ) {
		// Save the string
		str = new String( s );
		pointer = 0;
	}
	
	public boolean hasNextInt() {
		//Apply changes only on the auxiliary, not to modify the real pointer of the string
		int aux = pointer;
		//Skip the non-digit characters
		while( aux < str.length() && Character.isDigit( str.charAt( aux ) ) == false ) {
			aux++;
		}
		//Check if the next character is digit, or end
		if( aux < str.length() && Character.isDigit( str.charAt( aux ) ) == true )
			return true;
		return false;
	}
	
	public int nextInt() {
		//Skip the non-digit characters
		while( pointer < str.length() && Character.isDigit( str.charAt( pointer ) ) == false ) {
			pointer++;
		}
		int ans = 0;
		int sign = 1;
		//Check if the number is a negative one, otherwise it is positive
		if( pointer != 0 && str.charAt( pointer - 1 ) == '-' )
			sign = -1;
		//Parse the string in order to reconstruct the integer
		while( pointer < str.length() && Character.isDigit( str.charAt( pointer ) ) == true ) {
			ans = ans * 10 + str.charAt( pointer ) - '0';
			pointer++;
		}
		return ans * sign;
	}
}
