package ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tp.Polynomial;

public class Gui implements ActionListener {
	
	private JTextField firstPolynomial, secondPolynomial, result;
	
	public Gui() {
		//Creating the main frame representing the screen
		JFrame frame = new JFrame( "PT Assignament1" );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setSize( 600 ,  500 );
		
		//Creating a panel with BoxLayout in order to arrange the elements vertically in the screen
		JPanel panel = new JPanel();
		panel.setLayout( new BoxLayout( panel , BoxLayout.PAGE_AXIS ) );
		frame.add( panel );
		
		//Creating the JLabels and JTextFields for the input section of the interface
		firstPolynomial = new JTextField();
		firstPolynomial.setMaximumSize( new Dimension( 500 , 50 ) );
		secondPolynomial = new JTextField();
		secondPolynomial.setMaximumSize( new Dimension( 500 , 50 ) );
		
		JLabel firstPol = new JLabel( "First polynomial: " );
		JLabel secondPol = new JLabel( "Second polynomial: " );
		//Adding this input components to the panel
		panel.add( firstPol );
		panel.add( firstPolynomial );
		panel.add( secondPol );
		panel.add( secondPolynomial );
		
		//Create a panel with a FlowLayout to arrange the elements in the same row.
		JPanel buttons = new JPanel( new FlowLayout() );
		//Crate the buttons and add them to the buttons panel
		JButton plus = new JButton( "+" );
		JButton minus = new JButton( "-" );
		JButton multiply = new JButton( "*" );
		JButton divide = new JButton( "/" );
		JButton integrate = new JButton( "�" );
		JButton derivate = new JButton( "dx" );
		
		buttons.add(plus);
		buttons.add(minus);
		buttons.add(multiply);
		buttons.add(divide);
		buttons.add(integrate);
		buttons.add(derivate);
		
		//Adding the action listener to them
		plus.addActionListener( this );
		minus.addActionListener( this );
		multiply.addActionListener( this );
		divide.addActionListener( this );
		integrate.addActionListener( this );
		derivate.addActionListener( this );

		//Creating the result section of the screen
		JLabel res = new JLabel( "Result:" );
		result = new JTextField();
		result.setMaximumSize( new Dimension( 500 , 50 )  );
		
		//Putting all together to describe the full screen
		panel.add( buttons );
		panel.add( res );
		panel.add( result );
		frame.setVisible( true );
		
		
		
	}
	
	public static void main( String args[] ) {
		Gui gui = new Gui();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//Check the text in the operation
		String op = ((JButton)(arg0.getSource())).getText();
		//Get the input needed by the calculation enterd by the user
		Polynomial a = new Polynomial( firstPolynomial.getText() );
		Polynomial b = new Polynomial( secondPolynomial.getText() );
		String ans = new String();
		//According to the operation we calculate, then display the result
		if( op == "+" ) {
			a.add( b );
		} else if( op == "-" ) {
			a.subtract( b );
		} else if( op == "*" ) {
			a.multiply( b );
		} else if( op == "/" ) {
			ans = "Remainder: ** " + a.divide( b ).toString() + " ** Result: ";
		} else if( op == "dx" ) {
			a.derivate();
		} else {
			a.integrate();
		}
		ans = ans + a.toString();
		result.setText(ans);
	}
}
