package tp;

public class Monomial {
	private int degree;
	private double coefficient;
	
	private static double fastPow( double x, int p ) {
		//Calculate x to the power of p in logN time
		if( p == 0 ) return 1.0;
		if( p == 1 ) return x;
		
		double ans = fastPow( x , p / 2 );
		ans = ans * ans;
		if( p % 2 == 1 ) {
			ans = ans * x;
		}
		return ans;
	}
	
	public Monomial(){
		//If no parameters are send, create an "empty" monomial
		degree = 0;
		coefficient = 0;
	}
	
	public Monomial( int degree, double coefficient ) {
		//Initialize the monomial with the given degree and coefficient
		this.degree = Math.max( 0 , degree );
		this.coefficient = coefficient;
	}
	
	public Monomial( Monomial monomial ) {
		//Create a copy of the monomial, for easier manipulation and temporary modifications and calculations
		degree = monomial.getDegree();
		coefficient = monomial.getCoefficient();
	}
	
	public double evaluateIn( double point ) {
		//Calculate the value of the monomial in a certain point
		return fastPow( point , degree ) * coefficient;
	}
	
	public void integrate() {
		//Rule for the monomial antiderivative
		degree++;
		coefficient = coefficient / degree;
	}
	
	public void derivate() {
		//Rule for the monomial derivative
		coefficient = coefficient * degree;
		degree--;
	}
	
	public void add( Monomial monomial ) {
		//Check if the degree is the same, and add the coefficients if so
		if( monomial.getDegree() == this.degree ) {
			this.coefficient += monomial.getCoefficient();
		}
	}
	
	public void subtract( Monomial monomial ) {
		//Check if the degree is the same, and subtract the coefficients if so
		if( monomial.getDegree() == this.degree ) {
			this.coefficient -= monomial.getCoefficient();
		}
	}
	
	public void multiply( Monomial monomial ) {
		//To multiply two monomials, add the degrees and multiply the coefficients
		this.degree += monomial.getDegree();
		this.coefficient *= monomial.getCoefficient();
	}
	
	public void divide( Monomial monomial ) {
		//To divide two monomials, subtract the degrees and divide the coefficients
		//This operation is valid only if the degree of the first operand is greater or equal to the second operand degree
		if( this.degree >= monomial.getDegree() ) {
			this.degree -= monomial.getDegree();
			this.coefficient /= monomial.getCoefficient();
		}
	}
	
	public int getDegree() {
		return degree;
	}
	
	public double getCoefficient() {
		return coefficient;
	}
	
	public String toString() {
		String ans = new String();
		if( coefficient >= 0 ) {
			ans = "+";
		}
		return ans + coefficient + "x^" + degree;
	}
	
}
