package tp;

import java.util.Map;
import java.util.TreeMap;

import algorithms.FancyScanner;

public class Polynomial {
	
	private TreeMap< Integer , Monomial > monomials;
	
	
	public Polynomial(){
		//Initialize the collection of monomials
		monomials = new TreeMap< Integer , Monomial >();
	}
	
	public Polynomial( Polynomial polynomial ) {
		//Create an empty monomial collection and add the parameter polynomial
		monomials = new TreeMap< Integer , Monomial >();
		add( polynomial );
	}
	
	public Polynomial( String polynomial ) {
		//Create a fancy scanner to parse the input
		monomials = new TreeMap< Integer , Monomial >();
		FancyScanner scanner = new FancyScanner( polynomial.replace('x', ' ').replace('^', ' ') );
		while( scanner.hasNextInt() ) {
			//Get coefficient-degree pairs
			int coefficient = scanner.nextInt();
			int degree = 0;
			if( scanner.hasNextInt() ) {
				degree = scanner.nextInt();
			}
			this.add( new Monomial( degree , coefficient ) );
		}
	}
	
	public boolean equals( Polynomial polynomial ) {
		//Check if two polynomials are the same
		Polynomial ans = new Polynomial( this );
		ans.subtract( polynomial );
		if( ans.monomials.size() != 0 )
			return false;
		return true;
	}
	
	public void add( Monomial monomial ) {
		//Get the monomial with the same degree
		Monomial acc = monomials.get( new Integer( monomial.getDegree() ) );
		if( acc == null ){
			//If such monomial does not exist, add it as it is
			monomials.put( new Integer( monomial.getDegree() ) , new Monomial( monomial ) );
		} else {
			//If exists, add the coefficients
			acc.add( monomial );
			//Delete if coefficient is 0
			if( acc.getCoefficient() <= 0.000001 ) {
				monomials.remove( new Integer( acc.getDegree() ) );
			}
		}
	}

	public void add( Polynomial polynomial ) {
		//Add every monomial to the polynomial
		for( Map.Entry< Integer , Monomial > monomial : polynomial.monomials.entrySet() ) {
			add( monomial.getValue() );
		}
	}
	
	public void subtract( Monomial monomial ) {
		//Add the monomial with a negative sign coefficient
		add( new Monomial( monomial.getDegree() , -monomial.getCoefficient() ) );
	}
	
	public void subtract( Polynomial polynomial ) {
		//Subtract every monomial from the polynomial
		for( Map.Entry< Integer , Monomial > monomial : polynomial.monomials.entrySet() ) {
			subtract( monomial.getValue() );
		}
	}
	
	public void multiply( Monomial monomial ) {
		//Multiply the monomial with each monomial from the collection of monomials from the current polynomial
		Polynomial polynomial = new Polynomial();
		polynomial.add( monomial );
		this.multiply( polynomial );
	}
	
	
	public void multiply( Polynomial polynomial ) {
		//Multiply each monomial with the polynomial
		Polynomial ans = new Polynomial();
		for( Map.Entry< Integer , Monomial > monomial1 : polynomial.monomials.entrySet() ) {
			for( Map.Entry< Integer , Monomial > monomial2 : monomials.entrySet() ) {
				Monomial answer = new Monomial( monomial1.getValue() );
				answer.multiply( monomial2.getValue() );
				ans.add( answer );
			}
		}
		monomials = ans.monomials;
	}
	
	public Polynomial divide( Polynomial polynomial ) {
		//Get the two highest degree monomials from the two polynomials
		Map.Entry< Integer , Monomial > dispenser = this.monomials.lastEntry();
		Map.Entry< Integer , Monomial > dividend = polynomial.monomials.lastEntry();
		//Create an empty polynomial for the result
		Polynomial result = new Polynomial();
		Polynomial remainder = new Polynomial();
		while( dispenser != null && dividend != null && dispenser.getValue().getDegree() >= dividend.getValue().getDegree() ) {
			
			Monomial term = new Monomial( dispenser.getValue() );
			//Get the division monomial and add it to the result
			term.divide( dividend.getValue() );
			result.add( term );
			//Multiply it with the second operand
			Polynomial aux = new Polynomial( polynomial );
			aux.multiply( term );
			//Subtract it from the first operand
			this.subtract( aux );
			dispenser = this.monomials.lastEntry();
		}
		remainder = new Polynomial ( this );
		monomials = new TreeMap< Integer , Monomial >();
		this.add( result );
		return remainder;
	}
	
	public void derivate() {
		//Derivate all the monomials, if there is a monomial with degree 0, a constant, it gets removed
		if( monomials.get( new Integer( 0 ) ) != null ){
			monomials.remove( new Integer( 0 ) );
		}
		for( Map.Entry< Integer , Monomial > monomial : monomials.entrySet() ) {
			monomial.getValue().derivate();
		}
	}
	
	public void integrate() {
		//Integrate all the monomials in the polynomial
		for( Map.Entry< Integer , Monomial > monomial : monomials.entrySet() ) {
			monomial.getValue().integrate();
		}
	}
	
	public String toString() {
		String ans = new String();
		for( Map.Entry< Integer, Monomial > monomial : monomials.entrySet() ) {
			ans = monomial.getValue() + ans;
		}
		return ans;
	}
	
}
